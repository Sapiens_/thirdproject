<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>thirdLab</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
    <div id="form" class="col-12 order-first order-md-2 px-0 py-4">
        <h3>Форма</h3>
        <form action="" method="POST">
            <label>Ваше имя - <br>
                <input name="name" placeholder="Имя :" />
            </label><br />
            <label>
                Ваш email - <br />
                <input name="email" placeholder="test@example.com" type="email" />
            </label><br />
            <label>
                Год рождения:<br />
                <select name="year">
                    <?php for($i = 1900; $i < 2021; $i++) {?>
                    <option value="<?php print $i; ?>"><?= $i; ?></option>
                    <?php }?>
                </select>
            </label><br />
            <label>
                Пол - <br />
                <input type="radio" checked="checked" name="sex" value="Значение1" />
                М
                <input type="radio" name="sex" value="Значение2" />
                Ж
            </label><br />
            <label>
                Количество конечностей -<br />
                <input type="radio" checked="checked" name="limb" value="1" />
                1
                <input type="radio" name="radio-group-1" value="2" />
                2
                <br />
                <input type="radio" name="radio-group-1" value="3" />
                3
                <input type="radio" name="radio-group-1" value="4" />
                4
            </label><br />
            <label>
                Биография - <br />
                <textarea name="bio"></textarea>
            </label><br />
            <label>
                Сверхспособности -
                <br />
                <select name="power[]" multiple="multiple">
                    <option value="god"> бессмертие</option>
                    <option value="clip">прохождение сквозь стены</option>
                    <option value="fly">левитация</option>
                </select>
            </label><br />
            <label>
                <input type="checkbox" checked="checked" name="check-1" />
                С контрактом ознакомлен<br />
                <input type="submit" value="Отправить" />
            </label>
        </form>
    </div>
</body>

</html>
